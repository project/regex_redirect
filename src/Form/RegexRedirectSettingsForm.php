<?php

namespace Drupal\regex_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for configuring the regex_redirect module.
 *
 * @package Drupal\regex_redirect\Form
 */
class RegexRedirectSettingsForm extends ConfigFormBase {

  /**
   * Settings const.
   *
   * @var string
   */
  const SETTINGS = 'regex_redirect.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'regex_redirect_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['blacklisted_source_paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Source Path Blacklist'),
      '#description' => $this->t('Paths to skip for redirecting to another page. Input multiple values with a space in between: /admin /node'),
      '#default_value' => implode(' ', $config->get('blacklisted_source_paths')),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $urls = explode(' ', $form_state->getValue('blacklisted_source_paths'));
    foreach ($urls as $url) {
      if (preg_match('/^\/([\w-]+)$/', $url) !== 1) {
        $form_state->setErrorByName(
          'blacklisted_source_paths',
          $this->t('The value :path does not match the format: /path', [':path' => $url])
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('blacklisted_source_paths', explode(' ', $form_state->getValue('blacklisted_source_paths')))
      ->save();
  }

}

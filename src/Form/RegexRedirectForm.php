<?php

namespace Drupal\regex_redirect\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\MatchingRouteNotFoundException;
use Drupal\Core\Url;
use Drupal\regex_redirect\Entity\RegexRedirect;

/**
 * Form for creating or editing a new regex redirect.
 *
 * @package Drupal\regex_redirect\Form
 */
class RegexRedirectForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\Routing\MatchingRouteNotFoundException
   */
  protected function prepareEntity() {
    /** @var \Drupal\regex_redirect\Entity\RegexRedirect $redirect */
    $redirect = $this->entity;

    if ($redirect->isNew()) {
      $this->setDefaultOptions($redirect);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\regex_redirect\Entity\RegexRedirect $redirect */
    $redirect = $this->entity;

    if (!$redirect->isNew()) {
      $title = $redirect->get('title')->getString();
      $language = $redirect->get('language')->getString();
    }
    else {
      $title = '';
      $language = regex_redirect_language_code_default();
    }

    $form = parent::form($form, $form_state);

    $default_code = $redirect->getDefaultStatusCode();

    $form['regex_message'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'The entire source path should be a regular expression. For example: node\/(?P<name>[0-9a-z\.]+). The redirect itself should contain the specified captured name within <> brackets.'
      ),
      '#weight' => -12,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The regex redirect title used to identify the url starting points.'),
      '#default_value' => $title,
      '#required' => TRUE,
      '#weight' => -10,
    ];

    $form['regex_redirect_source']['widget'][0]['path']['#title'] = $this->t('Redirect source');
    $form['regex_redirect_source']['widget'][0]['path']['#description'] =
      $this->t('Enter an internal url regex path with named captures. For example: page\/old\/(?P&lt;nr&gt;[0-9a-z]+). Please be aware that certain characters such as "$" need to be escaped due to it being part of a replacement string.');
    $form['regex_redirect_source']['widget'][0]['path']['#size'] = 120;
    $form['redirect_redirect']['widget'][0]['uri']['#description'] =
      $this->t('Enter an internal url path with a leading slash and named capture variables between angular brackets. For example: /page/new/&lt;nr&gt;.');
    $form['redirect_redirect']['widget'][0]['uri']['#size'] = 120;
    $form['status_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirect status'),
      '#default_value' => $default_code,
      '#options' => regex_redirect_status_code_options(),
    ];

    // Additional langcode field with only the configured languages to set
    // the entity language.
    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Regex redirect language'),
      '#languages' => LanguageInterface::STATE_CONFIGURABLE,
      '#default_value' => $language,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\Exception\UndefinedLinkTemplateException
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation on queries because no queries were specified as necessary
    // and easier code is better code. There will be no filtering on xss, and
    // only dangerous protocols will be stripped.
    $source = $form_state->getValue(['regex_redirect_source', 0]);
    $source_path = UrlHelper::stripDangerousProtocols($source['path']);
    $redirect = $form_state->getValue(['redirect_redirect', 0]);
    $redirect_uri = UrlHelper::stripDangerousProtocols($redirect['uri']);

    $this->setBasicFormErrors($form_state, $source_path, $redirect_uri);
    $this->setRegexErrors($form_state, $source_path, $redirect_uri);
    $this->setBlackListErrors($form_state, $source_path);
    $this->hasDuplicates($form_state, $source_path);

    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\regex_redirect\Entity\RegexRedirect $redirect */
    $redirect = $this->entity;

    $redirect->setLanguage($form['langcode']['#value']);
    $redirect->setTitle($form['title']['#value']);

    $saved_status = $this->entity->save();
    $this->messenger()->addMessage($this->t('The redirect has been saved.'));
    $form_state->setRedirect('regex_redirect.list');

    return $saved_status;
  }

  /**
   * Set the default options on a new regex redirect entity.
   *
   * @param \Drupal\regex_redirect\Entity\RegexRedirect $redirect
   *   The regex redirect entity.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\Routing\MatchingRouteNotFoundException
   */
  protected function setDefaultOptions(RegexRedirect $redirect) {
    $source_url = urldecode($this->getRequest()->get('source') ?? '');
    if (!empty($source_url)) {
      $redirect->setSource($source_url);
    }

    if ($this->getRequest()->get('redirect_options')) {
      $redirect_options = $this->getRequest()->get('redirect_options');
      if (isset($redirect_options['query'])) {
        unset($redirect_options['query']);
      }
      $redirect_url = urldecode($this->getRequest()->get('regex_redirect'));
      if (!empty($redirect_url)) {
        try {
          $redirect->setRedirect($redirect_url, $redirect_options);
        }
        catch (MatchingRouteNotFoundException $e) {
          $this->messenger()->addWarning($this->t('Invalid regex redirect URL %url provided.', ['%url' => $redirect_url]));
        }
      }
    }

    $default_language = $redirect->language() !== NULL ? $redirect->language()->getId() : Language::LANGCODE_SITE_DEFAULT;
    $redirect->setLanguage($this->getRequest()->get('language') ?: $default_language);
  }

  /**
   * Do some basic form validation.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $source_path
   *   The source path.
   * @param string $redirect_uri
   *   The uri to redirect to.
   */
  protected function setBasicFormErrors(FormStateInterface $form_state, $source_path, $redirect_uri) {
    if ($source_path === '<front>') {
      $form_state->setErrorByName('regex_redirect_source', $this->t('It is not allowed to create a redirect from the front page.'));
    }
    if (strpos($source_path, '#') !== FALSE) {
      $form_state->setErrorByName('regex_redirect_source', $this->t('The anchor fragments are not allowed.'));
    }
    if (strpos($source_path, '/') === 0) {
      $form_state->setErrorByName('regex_redirect_source', $this->t('The url to redirect from should not start with a forward slash (/).'));
    }
    // The redirect url must be internal.
    if (strpos($redirect_uri, '/') !== 0) {
      $form_state->setErrorByName('redirect_redirect', $this->t('The url to redirect to should be internal, and thus must start with a forward slash (/).'));
    }
  }

  /**
   * Validate the regex pattern.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $source_path
   *   The source path.
   * @param string $redirect_uri
   *   The uri to redirect to.
   */
  protected function setRegexErrors(FormStateInterface $form_state, $source_path, $redirect_uri) {
    // Check whether redirect source is a valid regex pattern and whether
    // the redirect contains the named capture name in <> brackets.
    // Set the regex delimiter in code to avoid issues with leading slash.
    $delimiter = '/';
    $source_regex = $delimiter . trim($source_path) . $delimiter;
    $redirect_regex = str_replace('internal:', '', $redirect_uri);

    if (preg_match($source_regex, '') === FALSE) {
      $form_state->setErrorByName('regex_redirect_source', $this->t('The regex redirect source must be a regex pattern.'));
    }
    if (preg_match('/<(.*?)>/', $redirect_regex) !== 1) {
      $form_state->setErrorByName('redirect_redirect', $this->t('The regex redirect redirect must contain a named capture (value between <> brackets).'));
    }

    try {
      $source_url = Url::fromUri('internal:/' . $source_path);
      $redirect_url = Url::fromUri($redirect_uri);

      // It is relevant to do this comparison only in case the source path has
      // a valid route. Otherwise the validation will fail on the redirect path
      // being an invalid route.
      if ($source_url->toString() === $redirect_url->toString()) {
        $form_state->setErrorByName('redirect_redirect', $this->t('You are attempting to redirect the page to itself. This will result in an infinite loop.'));
      }
    }
    catch (\InvalidArgumentException $e) {
      // Do nothing, we want to only compare the resulting URLs.
    }
  }

  /**
   * Check if source has been blacklisted in configuration.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $source_path
   *   The source path.
   */
  protected function setBlackListErrors(FormStateInterface $form_state, $source_path) {
    $blacklisted_source_paths = parent::config('regex_redirect.settings')->get('blacklisted_source_paths');
    foreach ($blacklisted_source_paths as $path) {
      if (strpos($source_path, ltrim($path, '/')) === 0) {
        $form_state->setErrorByName('redirect_redirect', $this->t(
          'Value :url is blacklisted in the configuration and may therefore not be used as source.',
          [':url' => $source_path]
        ));

      }
    }
  }

  /**
   * Check for duplicates.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $source_path
   *   The source path.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\Exception\UndefinedLinkTemplateException
   */
  protected function hasDuplicates(FormStateInterface $form_state, $source_path) {
    // Generate the hash for checking duplicates.
    $hash = RegexRedirect::generateHash($source_path, $form_state->getValue('language')[0]['value']);

    // Search for duplicate.
    // EntityManagerInterface is deprecated, but this is inherited from
    // ContentEntityForm and should be fixed when that class is fixed.
    $redirects = $this->entityTypeManager->getStorage('regex_redirect')->loadByProperties(['hash' => $hash]);

    if (!empty($redirects)) {
      $redirect = array_shift($redirects);
      if ($this->entity->isNew() || $redirect->id() !== $this->entity->id()) {
        $form_state->setErrorByName(
          'regex_redirect_source',
          $this->t(
            'The source path %source is already being redirected. Do you want to <a href=":edit-page">edit the existing redirect</a>?',
            [
              '%source' => $source_path,
              ':edit-page' => $redirect->toUrl('edit-form'),
            ]
          )
        );
      }
    }
  }

}
